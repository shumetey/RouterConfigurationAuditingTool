from ciscoconfparse import CiscoConfParse
from ciscoconfparse.ccp_util import IPv4Obj
from netmiko import ConnectHandler
import os
from ciscoconfparse import CiscoPassword
#connection=ConnectHandler(ip="10.14.29.200 ",device_type="cisco_ios_telnet",username="R1",password="telnet")


iosv_l2 = {
    'device_type': 'cisco_ios_telnet',
    'ip': '10.14.30.200',          # removed
    'username': 'R1',              # removed
    'password': 'telnet',          # removed
    'secret':'telnet'
}

conn = ConnectHandler(**iosv_l2)
conn.enable()
a=conn.send_command("show startup")
# print(a);

#Creating file to be write
f = open("config.txt", "a")
f.write(a)
f.close()

# print(self.label.name)
parse = CiscoConfParse("config.txt", syntax='ios')
if __name__ == "__main__":
    confparse = CiscoConfParse("config.txt")
    # check if OSPF is used as the routing protocol
    # the following regex_pattern matches only the "router ospf &lt;process-id&gt;" command (no VRFs)
    ospf_regex_pattern = r"^router ospf \d+$"
    # in this case, we will simply check that the ospf router command is part of the config
    is_ospf_in_use = confparse.has_line_with(ospf_regex_pattern)
    if is_ospf_in_use:
        print("OSPF is used in this configuration")
    else:
        print(" OSPF is not used in this configuration")
        
#for intf_obj in parse.find_objects_w_child('^interface', '^\s+shutdown'):
#    print( intf_obj.text+":Shutdown")
    

# for practiceing 
parse = CiscoConfParse("config.txt", syntax='ios')
# obj_list = parse.find_objects_dna(r'hostname')
# print(obj_list[0].hostname)
archive= parse.find_all_children('^archive')
print( archive)
block=parse.find_blocks(' class class-default')
print(block);
password1= parse.find_all_children('^line ')
print(password1);
dp = CiscoPassword()
decrypted_passwd = dp.decrypt('15060E000A2F3F')
print(decrypted_passwd)



for intf_obj in parse.find_objects('^interface'):
    intf_name = intf_obj.re_match_typed('^interface\s+(\S.+?)$')
    # Search children of all interfaces for a regex match and return
    # the value matched in regex match group 1.  If there is no match,
    # return a default value: ''
    intf_ip_addr = intf_obj.re_match_iter_typed(
        r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)\s', result_type=str,
        group=1, default='Shutdawn')
    print("{0}: {1}".format(intf_name, intf_ip_addr))

#Deleting file that has been created
if os.path.exists("config.txt"):
  os.remove("config.txt")
else:
  print("The file does not exist!")


  